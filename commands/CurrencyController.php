<?php

namespace app\commands;

use app\models\db\Currency;
use yii\base\InvalidConfigException;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Class CurrencyController
 * @package app\commands
 */
class CurrencyController extends Controller
{
    /**
     * @param $code
     * @return int
     * @throws InvalidConfigException
     */
    public function actionSync($code)
    {
        if($currency = Currency::findOne(['char_code' => $code])) {
            if($currency->sync()) {
                echo "{$currency->char_code} is updated" . PHP_EOL;
                return ExitCode::OK;
            } else {
                echo "{$currency->char_code} isn't updated" . PHP_EOL;
                return ExitCode::UNSPECIFIED_ERROR;
            }
        }

        echo "{$code} was not found" . PHP_EOL;
        return ExitCode::NOINPUT;
    }

    /**
     * @param null $date
     * @return mixed
     * @throws InvalidConfigException
     */
    public function actionSyncAll($date = null)
    {
        $updated = Currency::syncAll($date);

        echo "Обновлено курсов валют: {$updated}" . PHP_EOL;

        return ExitCode::OK;
    }
}

