<?php

namespace app\components\cbr;

use DateTime;
use SimpleXMLElement;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\httpclient\Exception;

/**
 * Class CurrencyLoader
 * @package app\components\cbr
 *
 * @property array $currencies
 */
class CurrencyLoader extends Component
{
    public $url = 'http://www.cbr.ru/scripts/XML_daily.asp';

    public $date;

    /**
     * @var array
     */
    protected $_currencies;

    /**
     * @return array
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function getCurrencies()
    {
        if(!is_array($this->_currencies)) {
            $this->loadCurrencies();
        }

        return $this->_currencies;
    }

    /**
     *
     * @throws InvalidConfigException
     * @throws Exception
     */
    protected function loadCurrencies()
    {
        /** Создаем объект клиента */
        $client = new Client();

        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl([$this->url,'date_req' => $this->date])
            ->setFormat(Client::FORMAT_XML)
            ->send();

        /**
         * Получаем из строки в XML формате объект
         * @var string $xmlContent
         */
        $xmlContent = new SimpleXMLElement($response->content);

        /** Преобразуем в JSON */
        $jsonData = Json::encode($xmlContent);

        /** Преобразуем в ассоциативный массив */
        $data = Json::decode($jsonData);

        /** Формируем дату курсов валют от ЦБР */
        $date = DateTime::createFromFormat('d.m.Y', $data['@attributes']['Date']);

        /**
         * Перебираем в данных массив с курсами валюь
         * @var array $item
         */
        foreach ($data['Valute'] as $item) {

            /** Слздаем массив с данными о курсе валюты с указанием даты курса, полученной в ответе*/
            $currency = [
                'num_code'  => $item['NumCode'],
                'char_code' => $item['CharCode'],
                'nominal'   => (int)$item['Nominal'],
                'name'      => $item['Name'],
                'value'     => (float)str_replace(',', '.', $item['Value']),
                'date'      => date_format($date, 'Y-m-d'),
            ];

            /** Заполняем/добавляем в массив валют с ключом идентификатора валюты от ЦБР */
            $this->_currencies[$item['@attributes']['ID']] = $currency;
        }

        return $this->_currencies;
    }
}