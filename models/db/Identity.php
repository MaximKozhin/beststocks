<?php

namespace app\models\db;

use Yii;
use app\models\traits\IdentityTrait;
use app\models\traits\NonDeletableTrait;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "{{%identity}}".
 *
 * @property int $id
 * @property string $username Логин пользователя
 * @property string $password Хэш пароля
 * @property string $auth_key Ключ аутентификации
 * @property string $token Токен
 * @property int|null $deleted Удалено
 * @property int|null $created_at Создано
 * @property int|null $updated_at Изменено
 */
class Identity extends ActiveRecord implements IdentityInterface
{
    /** Реализация интерфейса IdentityInterface */
    use IdentityTrait;

    /** Строки не удаляются из БД */
    use NonDeletableTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%identity}}';
    }

    /**
     * @inheritdoc
     * @throws Exception
     */
    public function init()
    {
        $this->auth_key = Yii::$app->security->generateRandomString(32);
    }

    /**
     * @param $password
     * @throws Exception
     */
    public function generatePassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * @param $password
     * @return bool
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * @return array|array[]
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password'], 'required'],
            [['password'], 'string', 'min' => 5],
            [['username', 'auth_key'], 'trim'],
            [['deleted', 'created_at', 'updated_at'], 'integer'],
            [['username', 'auth_key', 'token'], 'string', 'max' => 100],
            [['username'], 'unique'],
            [['auth_key'], 'unique'],
            [['token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин пользователя',
            'password' => 'Пароль пользователя',
            'auth_key' => 'Ключ аутентификации',
            'token' => 'Токен',
            'deleted' => 'Удалено',
            'created_at' => 'Создано',
            'updated_at' => 'Изменено',
        ];
    }
}
