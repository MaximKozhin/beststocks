<?php

namespace app\models\db;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%rate}}".
 *
 * @property int $id
 * @property int|null $currency_id Валюта
 * @property float|null $value Курс
 * @property string|null $date Дата курса
 * @property int|null $deleted Удалено
 * @property int|null $created_at Создано
 * @property int|null $updated_at Изменено
 *
 * @property Currency $currency
 */
class Rate extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%rate}}';
    }

    /**
     * @return array|array[]
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['currency_id', 'deleted', 'created_at', 'updated_at'], 'integer'],
            [['value'], 'number'],
            [['date'], 'safe'],
            [['date'], 'unique', 'targetAttribute' => ['date', 'currency_id']],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'currency_id' => 'Валюта',
            'value' => 'Курс',
            'date' => 'Дата курса',
            'deleted' => 'Удалено',
            'created_at' => 'Создано',
            'updated_at' => 'Изменено',
        ];
    }

    /**
     * @return array|string[]
     */
    public function fields()
    {
        return ['id', 'currency_id', 'value', 'date'];
    }

    /**
     * Gets query for [[Currency]].
     *
     * @return ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::class, ['id' => 'currency_id']);
    }
}
