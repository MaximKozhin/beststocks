<?php

namespace app\models\db;

use app\components\cbr\CurrencyLoader;
use Yii;
use app\models\traits\NonDeletableTrait;
use yii\base\InvalidConfigException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%currency}}".
 *
 * @property int $id
 * @property string $valute_id Идентификатор валюты
 * @property string $num_code Числовой код валюты
 * @property string $char_code Символьный код валюты
 * @property string $name Наименование валюты
 * @property int|null $nominal Наименование валюты
 * @property int|null $deleted Удалено
 * @property int|null $created_at Создано
 * @property int|null $updated_at Изменено
 *
 * @property string $fullName
 * @property Rate[] $rates
 * @property Rate $rate
 */
class Currency extends ActiveRecord
{
    use NonDeletableTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%currency}}';
    }

    /**
     * @return array|array[]
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['valute_id', 'num_code', 'char_code', 'name'], 'required'],
            [['nominal', 'deleted', 'created_at', 'updated_at'], 'integer'],
            [['valute_id', 'num_code', 'char_code'], 'string', 'max' => 10],
            [['name'], 'string', 'max' => 255],
            [['valute_id'], 'unique'],
            [['num_code'], 'unique'],
            [['char_code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'valute_id' => 'Идентификатор валюты',
            'num_code' => 'Числовой код валюты',
            'char_code' => 'Символьный код валюты',
            'name' => 'Наименование валюты',
            'nominal' => 'Наименование валюты',
            'deleted' => 'Удалено',
            'created_at' => 'Создано',
            'updated_at' => 'Изменено',
        ];
    }

    /**
     * @return array|string[]
     */
    public function fields()
    {
        return ['id', 'valute_id', 'num_code', 'char_code', 'name', 'nominal', 'rate'];
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->nominal . ' ' . $this->name;
    }

    /**
     * Gets query for [[Rates]].
     *
     * @return ActiveQuery
     */
    public function getRates()
    {
        return $this->hasMany(Rate::class, ['currency_id' => 'id'])
            ->where(['deleted' => false])
            ->orderBy(['date' => SORT_DESC]);
    }

    /**
     * Gets query for [[Rate]].
     *
     * @return ActiveQuery
     */
    public function getRate()
    {
        return $this->hasOne(Rate::class, ['currency_id' => 'id'])
            ->where(['deleted' => false])
            ->orderBy(['date' => SORT_DESC]);
    }

    /**
     * Метод обновления текущей валюты с ежедневным курсом
     *
     * @return bool
     * @throws InvalidConfigException
     */
    public function sync()
    {
        /** @var CurrencyLoader $cbr */
        $cbr = Yii::$app->get('cbr');

        /** Проверяем наличие внешнего идентификатора в качестве ключа в массиве полученных данных */
        if(key_exists($this->valute_id, $cbr->currencies)) {

            /** Обновляем атрибуты текущей валюты */
            $this->setAttributes([
                'num_code'  => $cbr->currencies[$this->valute_id]['num_code'],
                'char_code' => $cbr->currencies[$this->valute_id]['char_code'],
                'name'      => $cbr->currencies[$this->valute_id]['name'],
                'nominal'   => $cbr->currencies[$this->valute_id]['nominal'],
            ]);

            /** Если валюта сохранена */
            if($this->save()) {

                /** Условие для поиска ежедневного курса */
                $condition = ['currency_id' => $this->id, 'date' => $cbr->currencies[$this->valute_id]['date']];

                /** Если курса нет, то сохдаем новую модель */
                if(($rate = Rate::findOne($condition)) === null) {
                    $rate = new Rate($condition);
                }

                /** Устанавливаем курс на дату */
                $rate->setAttribute('value', $cbr->currencies[$this->valute_id]['value']);

                return $rate->save();
            }

            return false;
        } else {
            return $this->delete();
        }
    }

    /**
     * @param null $date
     * @return int
     * @throws InvalidConfigException
     */
    public static function syncAll($date = null)
    {
        /** @var CurrencyLoader $cbr */
        $cbr = Yii::$app->get('cbr');

        if($date) {
            $cbr->date = $date;
        }

        $updated = 0;

        foreach ($cbr->currencies as $id => $data) {

            /** Проверяем наличие текущей валюты */
            if(($currency = self::findOne(['valute_id' => $id])) === null) {
                $currency = new Currency(['valute_id' => $id]);
            }

            if($currency->sync()) {
                $updated++;
            }
        }

        return $updated;
    }
}
