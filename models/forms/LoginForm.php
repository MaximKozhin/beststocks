<?php

namespace app\models\forms;

use app\models\db\Identity;
use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property Identity|null $identity This property is read-only.
 *
 * @property string $username
 * @property string $password
 * @property string $rememberMe
 */
class LoginForm extends Model
{
    public $username;

    public $password;

    public $rememberMe = true;

    private $_identity = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Имя пользователя',
            'password' => 'Пароль',
            'rememberMe' => 'Запомнить меня'
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     */
    public function validatePassword($attribute)
    {
        if (!$this->hasErrors()) {

            if (!$this->identity || !$this->identity->validatePassword($this->password)) {
                $this->addError($attribute, 'Неправильное имя пользователя или пароль');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->identity, $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return Identity|null
     */
    public function getIdentity()
    {
        if ($this->_identity === false) {
            $this->_identity = Identity::findOne(['username' => $this->username, 'deleted' => false]);
        }

        return $this->_identity;
    }
}
