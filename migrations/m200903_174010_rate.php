<?php

use yii\db\Migration;

/**
 * Class m200903_174010_rate
 */
class m200903_174010_rate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%rate}}', [
            'id'            => $this->primaryKey(10)->unsigned(),
            'currency_id'   => $this->integer(10)->unsigned()->comment('Валюта'),
            'value'         => $this->double()->comment('Курс'),
            'date'          => $this->date()->comment('Дата курса'),
            'deleted'       => $this->boolean()->defaultValue(0)->comment('Удалено'),
            'created_at'    => $this->integer(10)->unsigned()->comment('Создано'),
            'updated_at'    => $this->integer(10)->unsigned()->comment('Изменено'),
        ], $this->db->driverName === 'mysql' ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB' : null);

        $this->addForeignKey('fk-rate-currency', '{{%rate}}', 'currency_id', '{{%currency}}', 'id');

        $this->createIndex('index-rate-date', '{{%rate}}', 'date');
        $this->createIndex('index-rate-deleted', '{{%rate}}', 'deleted');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-rate-currency', '{{%rate}}');

        $this->dropTable('{{%rate}}');
    }
}
