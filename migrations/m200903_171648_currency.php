<?php

use yii\db\Migration;

/**
 * Class m200903_171648_currency
 */
class m200903_171648_currency extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%currency}}', [
            'id'            => $this->primaryKey(10)->unsigned(),
            'valute_id'     => $this->string(10)->notNull()->unique()->comment('Идентификатор валюты'),
            'num_code'      => $this->string(10)->notNull()->unique()->comment('Числовой код валюты'),
            'char_code'     => $this->string(10)->notNull()->unique()->comment('Символьный код валюты'),
            'name'          => $this->string(255)->notNull()->comment('Наименование валюты'),
            'nominal'       => $this->integer(10)->unsigned()->comment('Наименование валюты'),
            'deleted'       => $this->boolean()->defaultValue(0)->comment('Удалено'),
            'created_at'    => $this->integer(10)->unsigned()->comment('Создано'),
            'updated_at'    => $this->integer(10)->unsigned()->comment('Изменено'),
        ], $this->db->driverName === 'mysql' ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB' : null);

        $this->createIndex('index-currency-deleted', '{{%currency}}', 'deleted');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%currency}}');
    }
}
