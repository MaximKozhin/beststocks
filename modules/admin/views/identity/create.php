<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\db\Identity */

$this->title = 'Добавить пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="identity-create">
    <h1 class="no-margin clearfix">
        <?= Html::encode($this->title) ?>
    </h1>
    <hr>
    <div style="overflow: auto">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
