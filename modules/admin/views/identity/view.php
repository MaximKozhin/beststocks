<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\db\Identity */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="identity-view">

    <h1 class="no-margin clearfix">
        <?= Html::encode($this->title) ?>
        <div class="pull-right">
            <a href="<?=Url::to(['update', 'id' => $model->id])?>" class="btn btn-primary">
                <span class="glyphicon glyphicon-pencil"></span>
                <span class="hidden-xs hidden-sm">Редактировать</span>
            </a>
            <a href="<?=Url::to(['delete', 'id' => $model->id])?>" class="btn btn-danger" data-method="POST" data-confirm="Удалить?">
                <span class="glyphicon glyphicon-trash"></span>
                <span class="hidden-xs hidden-sm">Удалить</span>
            </a>
        </div>
    </h1>
    <hr>
    <div style="overflow: auto">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'username',
                'auth_key',
                'token',
                'deleted:boolean',
                'created_at:datetime',
                'updated_at:datetime',
            ],
        ]) ?>
    </div>
</div>
