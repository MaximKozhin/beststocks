<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\db\Identity */

$this->title = 'Редактировать пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="identity-update">
    <h1 class="no-margin clearfix">
        <?= Html::encode($this->title) ?>
    </h1>
    <hr>
    <div style="overflow: auto">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
