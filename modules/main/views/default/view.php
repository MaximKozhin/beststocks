<?php

use app\models\db\Currency;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $currency Currency */
?>
<div class="currency-view">
    <h1 class="no-margin clearfix">
        <?=Html::encode($this->title) ?>
        <div class="pull-right">
            <a href="<?=Url::to(['sync', 'id' => $currency->id])?>" class="btn btn-info" data-method="POST">
                <span class="glyphicon glyphicon-refresh"></span>
                <span class="hidden-xs hidden-sm">Обновить</span>
            </a>
            <a href="<?=Url::to(['delete', 'id' => $currency->id])?>" class="btn btn-danger" data-method="POST" data-confirm="Удалить?">
                <span class="glyphicon glyphicon-trash"></span>
                <span class="hidden-xs hidden-sm">Удалить</span>
            </a>
        </div>
    </h1>
    <hr>
    <div style="overflow: auto">
        <?= GridView::widget([
            'dataProvider' => new ActiveDataProvider(['query' => $currency->getRates()]),
            'rowOptions' => function ($model){
                return $model->deleted ? ['class' => 'danger'] : [];
            },
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'date:date',
                'value'
            ],
        ]); ?>
    </div>
</div>

