<?php

use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider ActiveDataProvider */

?>
<div class="main-index">
    <h1 class="no-margin clearfix">
        <?=Html::encode($this->title)?>
        <div class="pull-right ">
            <a href="<?= Url::to(['sync-all'])?>" class="btn btn-info" data-method="POST">
                <span class="glyphicon glyphicon-refresh"></span>
                <span class="hidden-sm hidden-xs">Обновить курсы</span>
            </a>
        </div>
    </h1>
    <hr>
    <div style="overflow: auto">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'rowOptions' => function ($model){
                return $model->deleted ? ['class' => 'danger'] : [];
            },
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'num_code',
                [
                    'attribute' => 'char_code',
                    'format' => 'html',
                    'value' => function ($model) {
                        return Html::a($model->char_code, ['view', 'id' => $model->id]);
                    }
                ],
                'nominal',
                'name',
                'rate.value'
            ],
        ]); ?>
    </div>
</div>