<?php

namespace app\modules\main\controllers;

use Yii;
use app\models\db\Currency;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class DefaultController
 * @package app\modules\main\controllers
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'sync' => ['POST'],
                    'sync-all' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $query = Currency::find()
            ->with(['rate'])
            ->where(['deleted' => false])
            ->orderBy(['char_code' => SORT_ASC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => new Pagination([
                'totalCount' => $query->count(),
                'pageSize' => 100
            ])
        ]);

        $this->view->title = Yii::$app->name;

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $currency = $this->findCurrency($id);

        $this->view->title = $currency->fullName;
        $this->view->params['breadcrumbs'][] = $currency->char_code;

        return $this->render('view', [
            'currency' => $currency
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function actionSync($id)
    {
        $currency = $this->findCurrency($id);

        if($currency->sync()) {
            Yii::$app->session->setFlash('success', 'Курс валюты успешно обновлен');
        } else {
            Yii::$app->session->setFlash('error', 'Курс валюты не обновлен');
        }

        return $this->redirect(['view', 'id' => $currency->id]);
    }

    /**
     * @return Response
     * @throws InvalidConfigException
     */
    public function actionSyncAll()
    {
        $updated = Currency::syncAll();

        Yii::$app->session->setFlash('success', "Обновлено курсов валют: {$updated}");

        return $this->redirect('index');
    }

    /**
     * @param $id
     * @return Currency|null
     * @throws NotFoundHttpException
     */
    protected function findCurrency($id)
    {
        if(($currency = Currency::findOne(['id' => $id, 'deleted' => false])) === null) {
            throw new NotFoundHttpException('Валюта не найдена и/или удалена');
        }
        return $currency;
    }
}