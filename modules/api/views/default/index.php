<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="api-index">
    <h1 class="no-margin clearfix">
        <?= Html::encode($this->title)?>
    </h1>
    <hr>
    <p>Для доступа к API используйте указанные данные и ссылки</p>
    <dl class="dl-horizontal">
        <dt>Bearer токен:</dt>
        <dd><?=Yii::$app->user->identity->token?></dd>
        <dt>Все курсы валют:</dt>
        <dd>
            <a href="<?= Url::toRoute(['currency/index'])?>">
                <?=Yii::$app->urlManager->createAbsoluteUrl('/api/currency/index')?>
            </a>
        </dd>
        <dt>Один курс валюты:</dt>
        <dd>
            <a href="<?= Url::to(['currency/view', 'id' => 0])?>">
                <?=Yii::$app->urlManager->createAbsoluteUrl(['/api/currency/view', 'id' => 0])?>
            </a>
        </dd>
        <dt>История курсов:</dt>
        <dd>
            <a href="<?= Url::to(['currency/rates'])?>">
                <?=Yii::$app->urlManager->createAbsoluteUrl(['/api/currency/rates'])?>
            </a>
        </dd>
    </dl>
</div>
