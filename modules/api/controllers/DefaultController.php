<?php

namespace app\modules\api\controllers;

use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class DefaultController
 * @package app\modules\api\controllers
 */
class DefaultController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->view->title = 'API';

        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('index');
    }
}