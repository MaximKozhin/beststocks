<?php

namespace app\modules\api\controllers;

use app\models\db\Currency;
use app\models\db\Rate;
use yii\base\DynamicModel;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\ContentNegotiator;
use yii\filters\RateLimiter;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class CurrencyController
 * @package app\modules\api\controllers
 */
class CurrencyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                    'application/xml' => Response::FORMAT_XML,
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET'],
                    'view' => ['GET'],
                ],
            ],
            'authenticator' => [
                'class' => HttpBearerAuth::class,
            ],
            'rateLimiter' => [
                'class' => RateLimiter::class,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'app\modules\api\actions\IndexAction',
                'modelClass' => Currency::class
            ],
            'view' => [
                'class' => 'yii\rest\ViewAction',
                'modelClass' => Currency::class
            ],
            'rates' => [
                'class' => 'app\modules\api\actions\IndexAction',
                'modelClass' => Rate::class,
                'dataFilter' => [
                    'class' => 'yii\data\DataFilter',
                    'filterAttributeName' => 'filter',
                    'searchModel' =>  function () {
                        return (new DynamicModel(['currency_id']))->addRule('currency_id', 'integer');
                    },
                ]
            ],
        ];
    }
}