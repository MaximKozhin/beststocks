<?php

namespace app\modules\api\actions;

use yii\data\ActiveDataProvider;
use yii\data\DataFilter;
use yii\rest\IndexAction as RestIndexAction;

/**
 * Class IndexAction
 * @package app\modules\api\actions
 */
class IndexAction extends RestIndexAction
{
    /**
     * @return void|ActiveDataProvider|DataFilter|null
     */
    protected function prepareDataProvider()
    {
        $dataProvider = parent::prepareDataProvider();

        $dataProvider->query->andWhere(['deleted' => false]);

        return $dataProvider;
    }
}