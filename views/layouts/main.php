<?php

/* @var $this View */
/* @var $content string */

use yii\helpers\Url;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<nav class="navbar navbar-inverse navbar-fixed-top" id="nav">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?=Yii::$app->homeUrl?>">
                <?=Yii::$app->name?>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <?php if(Yii::$app->user->id):?>
                    <li>
                        <a href="<?=Url::to(['/admin/identity/index'])?>">
                            <span class="glyphicon glyphicon-user"></span>
                            <span>Пользователи</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=Url::to(['/api/default/index'])?>">
                            <span class="glyphicon glyphicon-user"></span>
                            <span>API</span>
                        </a>
                    </li>
                    <li>
                        <a data-method="POST" href="<?=Url::to(Yii::$app->user->logoutUrl)?>" title="Выход">
                            <span>Выход</span>
                        </a>
                    </li>
                <?php else:?>
                    <li>
                        <a href="<?=Url::to(Yii::$app->user->loginUrl)?>" title="Войти">
                            <span>Вход</span>
                        </a>
                    </li>
                <?php endif?>
            </ul>
        </div>
    </div>
</nav>
<div class="wrap" id="wrap">
    <div class="container">
        <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs'] ?? []]) ?>
        <div class="flash-messages">
            <?= Alert::widget()?>
        </div>
    </div>
    <div class="content">
        <div class="container">
            <?= $content ?>
        </div>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?=Yii::$app->name?> <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
